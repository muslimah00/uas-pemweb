<head>
	<title>UAS</title>
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/font-awesome.min.css">
  
	<script src="/js/jquery-3.5.1.min.js"></script>
  
	<script src="/js/bootstrap.min.js"></script>
  
  <link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.css">
  <script type="text/javascript" charset="utf8" src="/js/jquery.dataTables.js"></script>
  
  <script src="https://d3js.org/d3.v6.min.js"></script>
  
  <script type="text/javascript" charset="utf8" src="/js/render.js"></script>
</head>