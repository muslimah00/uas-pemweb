<?php
	// Asumsi gunakan root untuk username database
	DEFINE('DB_USER', 'root');
	DEFINE('DB_PASSWORD', '');

	// Gunakan MySQL dengan nama database dbkalbar
	$data_source = 'mysql:host=localhost;dbname=dbkalbar';

	// Coba koneksi ke MySQL
	// PDO di sini supaya bisa akses database dengan PHP
	try {
		$database = new PDO($data_source, DB_USER, DB_PASSWORD);
	} catch (PDOException $e) { // Kalau gagal
		// Dapatkan pesan errornya kemudian tampilkan errornya di file database_error.php
		$error_message = $e -> getMessage();
		include('database_error.php');
		exit();
	}
?>


