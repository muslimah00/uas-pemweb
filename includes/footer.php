<!-- footer -->
<section id="footer">
  <img src="/img/wave2.png" class="footer-img">
  <div class="container">
  <div class="container-fuild">
    <div class="row">
      <div class="footer_blog">
        <div class="row">
          <div class="col-md-6">
            <div class="main-heading left_text">
              <h2>Tentang Kami</h2>
            </div>
            <p></p>

          </div>
          <!-- <div class="col-md-6">
            <div class="main-heading left_text">
              <h2>Hubungi Kami</h2>
            </div>
            <p>PROGRAM STUDI TEKNIK INFORMATIKA<br>
              Pontianak, Indonesia<br>
              <span style="font-size:18px;"><a href="(0561)740185>+(0561)740185</a></span><br>
              info@informatika.untan.ac.id</p>
          </div> -->

          <!-- footer -->
          <section id="footer">
            <div class="container">
              <div class="row">
                <div class="col-md-4 footer-box">
                  <img src="/img/logo.png">
                  <p>KANTOR GUBERNUR KALIMANTAN BARAT<br>Jl. Jenderal Ahmad Yani, Bangka Belitung Laut, Kec. Pontianak Tenggara, Kota Pontianak,<br>Kalimantan Barat 78124.</p>
                </div>
                <div class="col-md-4 footer-box">
                  <p><b>CONTACT US</b></p>
                  <P><i class="fa fa-map-marker"></i> Kalimantan Barat,Indonesia</P>
                  <p><i class="fa fa-phone"></i> (0561)740185</p>
                  <P><i class="fa fa-envelope-o"></i>info@informatika.untan.ac.id</P>
              </div>
              <div class="col-md-4 footer-box">
                <p><b>SUBSCRIBE NEWSLETTER</b></p>
                <input type="email" class="form-control" placeholder="Your Email">
                <button type="button" class="btn btn-primary">Subcribe</button>
              </div>
            </div>
          </section>
          
        </div>
      </div>
    </div>
  </div>
  </div>
  <hr>
  <p class="copyright">© 2020 CFMS Inc.</p>
  </div>
</div>
</section>

<!-- smooth scroll -->
<script src="/js/smooth-scroll.js"></script>
<script>
  var scroll = new SmoothScroll('a[href*="#"]');
</script>