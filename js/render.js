function render(year, base="ff") {
  var dict = {
  "Sambas": "#sambas",
  "Bengkayang": "#bengkayang",
  "Landak": "#landak",
  "Sanggau": "#sanggau",
  "Ketapang": "#ketapang",
  "Sintang": "#sintang",
  "Kapuas Hulu": "#kapuashulu",
  "Sekadau": "#sekadau",
  "Melawi": "#melawi",
  "Kayong Utara": "#kayongutara",
  "Kubu Raya": "#kuburaya",
  "Kota Pontianak": "#kotapontianak",
  "Kota Singkawang": "#kotasingkawang"
  };
  
  var res = res_all[year];
      
  var min_range = res[0][1];
  var max_range = res[0][1];


  for (var i = 0; i < res.length; i++) {
    min_range = Math.min(min_range, res[i][1]);
    max_range = Math.max(max_range, res[i][1]);
  }

  function norm(n) {
    if (max_range-min_range == 0) {
      return 1;
    } else {
      return (n-min_range)/(max_range-min_range);
    }
  }

  var intensity = 250;

  function convert(integer) {
    var str = Number(integer).toString(16);
    return str.length == 1 ? "0" + str : str;
  }
  
  for (var key in dict) {
    if (dict.hasOwnProperty(key)) {           
      d3.selectAll(dict[key]).style("fill", "#dcdcdc");
      d3.selectAll("g"+dict[key]).selectAll("*").style("fill", "#dcdcdc");
    }
  }
  
  for (var i = 0; i < res.length; i++) {
    var color_string = convert(Math.floor(intensity-intensity*norm(res[i][1])));
    d3.selectAll(dict[res[i][0]]).style("fill", "#"+base+color_string+color_string);
    d3.selectAll("g"+dict[res[i][0]]).selectAll("*").style("fill", "#"+base+color_string+color_string);
  }
}
