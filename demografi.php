<?php
// kita pakai $database yang ada di database_connect.php
require('includes/database_connect.php');

// spesifikasi query untuk tabel demografi penduduk
$query_tabel_demografipenduduk = 'SELECT * FROM tb_demografipenduduk ORDER BY No ASC';

// siapkan statement dengan query yang udah ada
$statement = $database->prepare($query_tabel_demografipenduduk);

// jalankan statement dan simpan hasilnya di hasil_query_demografipenduduk
$statement->execute();

// hasil query ini akan digunakan nanti
$hasil_query_demografipenduduk = $statement->fetchAll();

// supaya statement yang lain dapat dieksekusi, 
// statement sebelumnya harus diclose terlebih dahulu
$statement->closeCursor();

$query_tabel_tahun = 'SELECT DISTINCT tahun FROM tb_demografipenduduk ORDER BY tahun ASC';
$statement = $database->prepare($query_tabel_tahun);
$statement->execute();
$hasil_query_tahun = $statement->fetchAll();
$statement->closeCursor();
?>

<!DOCTYPE html>
<html>
<?php require_once('includes/header.php') ?>
<body>
	<?php require_once('includes/navbar.php') ?>
  
  <div class="container" id="svgEmbed" style="align: center">
    <h3>Kepadatan penduduk pada Kalimantan Barat</h3>
    <select id="tahun" class="form-control">
      <?php foreach($hasil_query_tahun as $baris) : ?>
        <option value="<?php echo $baris['tahun'] ?>"><?php echo $baris['tahun'] ?></option>
      <?php endforeach; ?>
    </select>
  </div>
  
	<!-- main contents -->
	<section id="contents">
		<h3 align="center">Tabel Demografi</h3> 
		<div class="container">
			<table class="table table-hover" id="resultTable">
				<thead>
				<tr>
					<th>No</th>
					<th>ID kelompok</th>
					<th>Nama kabupaten</th>
					<th>Tahun</th>
					<th>Jumlah laki-laki</th>
					<th>Jumlah perepmpuan</th>
					<th>Jumlah total</th>
          <th>Laju pertumbuhan (%)</th>
          <th>Luas wilayah (km^2)</th>
          <th>Kepadatan</th>
          <th>Sumber data</th>
					</thead>
				</tr>
				<tbody>
				<?php foreach($hasil_query_demografipenduduk as $baris) : ?>
				<tr>
					<!-- sesuai nama yang ada di database -->
					<td><?php echo $baris['No']; ?></td>
					<td><?php echo $baris['id_kel']; ?></td>
					<td><?php echo $baris['nama_kab']; ?></td>
					<td><?php echo $baris['tahun']; ?></td>
					<td><?php echo $baris['Laki_Laki']; ?></td>
					<td><?php echo $baris['Perempuan']; ?></td>
          <td><?php echo $baris['Laki_Laki']+$baris['Perempuan']; ?></td>
          <td><?php echo $baris['Laju_Pertumbuhan'] ?></td>
          <td><?php echo $baris['Luas_Wilayah'] ?></td>
          <td><?php echo $baris['Kepadatan'] ?></td>
          <td><?php echo $baris['sumber_data'] ?></td>
				</tr>
				<?php endforeach; ?>
				</tbody>
			</table>			
		</div>
	</section>
  
  <script type="text/javascript">
    var res_all = {};
    
    $(document).ready(async function() { 
      await d3.svg("/svg/kalbar.svg").then(function(xml) {
        d3.select("#svgEmbed").node().appendChild(xml.documentElement);
      });
      
      d3.select("svg").style("margin", "auto");
      d3.select("svg").style("display", "block");
      
      <?php 
        foreach($hasil_query_demografipenduduk as $baris): 
          $y = $baris['tahun'];
          $kab = $baris['nama_kab'];
          $val = $baris['Kepadatan'];
          echo "if ($y in res_all) { res_all[$y].push(['$kab', $val]); } else { res_all[$y] = [['$kab', $val]]; } ";
        endforeach; 
      ?>
      
      
      <?php 
        $last = $hasil_query_tahun[0]['tahun'];
        echo "render($last);"; 
      ?>
    });
    
    $("#tahun").change(function () {
        var end = this.value;
        render(end);
    });
    
  </script>
  <?php require_once('includes/loadresulttable.php') ?>
	<?php require_once('includes/footer.php') ?>
</body>
</html>
