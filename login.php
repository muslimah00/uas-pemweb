<?php
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }
  if(isset($_SESSION["admin"])) {
    header('Location: /'); 
    exit();
  }
  
  $plaintextPassword = filter_input(INPUT_POST, 'password');
  $incorrect = null;
  if ($plaintextPassword != null) {
    $hashedPassword = '$2y$10$3HLRe70qSp3IrV0KKgrwp.4q6qpR9laQ/BcowJ9cPXry8kh/isJk.';
    
    if (password_verify($plaintextPassword, $hashedPassword)) {
      $_SESSION["admin"] = true;
      header('Location: angkakemiskinan.php'); 
      exit();
    } else {
      $incorrect = true;
    }
    
  } else {
    $hashedPassword = null;
  }
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>َLogin</title>
        <link rel="stylesheet" href="/css/stylelogin.css">
    </head>
    <body>
        <div class="box">
            <form action="/login.php" method="post">
                <h1>Login</h1>
                <?php if ($incorrect) { echo '<h3 style="color: red">Invalid password</h3>'; } ?>
                <input type="password" name="password" placeholder="Password">
                <input type="submit" value="Login">
            </form>
            <a href='/' style="text-decoration:none"><button>Go back</button></a>
        </div>
    </body>
</html>