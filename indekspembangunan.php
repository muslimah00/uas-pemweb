<?php
// kita pakai $database yang ada di database_connect.php
require('includes/database_connect.php');

// spesifikasi query untuk tabel IPM
$query_tabel_ipm = 'SELECT * FROM tb_ipm ORDER BY No ASC';

// siapkan statement dengan query yang udah ada
$statement = $database->prepare($query_tabel_ipm);

// jalankan statement dan simpan hasilnya di hasil_query_ipm
$statement->execute();

// hasil query ini akan digunakan nanti
$hasil_query_ipm = $statement->fetchAll();

// supaya statement yang lain dapat dieksekusi, 
// statement sebelumnya harus diclose terlebih dahulu
$statement->closeCursor();

$query_tabel_tahun = 'SELECT DISTINCT tahun FROM tb_ipm ORDER BY tahun ASC';
$statement = $database->prepare($query_tabel_tahun);
$statement->execute();
$hasil_query_tahun = $statement->fetchAll();
$statement->closeCursor();
?>

<!DOCTYPE html>
<html>
<?php require_once('includes/header.php') ?>
<body>
	<?php require_once('includes/navbar.php') ?>
  
  <div class="container" id="svgEmbed" style="align: center">
    <h3>Indeks Pembangunan Manusia (IPM) pada Kalimantan Barat</h3>
    <select id="tahun" class="form-control">
      <?php foreach($hasil_query_tahun as $baris) : ?>
        <option value="<?php echo $baris['tahun'] ?>"><?php echo $baris['tahun'] ?></option>
      <?php endforeach; ?>
    </select>
  </div>
  
	<!-- main contents -->
	<section id="contents">
		<h3 align="center">Tabel Indeks Pembangunan Manusia (IPM)</h3> 
		<div class="container">
			<table class="table table-hover" id="resultTable">
				<thead>
				<tr>
					<th>No</th>
					<th>ID kelompok</th>
					<th>Nama kabupaten</th>
					<th>Tahun</th>
					<th>IPM</th>
          <th>Sumber data</th>
					</thead>
				</tr>
				<tbody>
				<?php foreach($hasil_query_ipm as $baris) : ?>
				<tr>
					<!-- sesuai nama yang ada di database -->
					<td><?php echo $baris['No']; ?></td>
					<td><?php echo $baris['id_kel']; ?></td>
					<td><?php echo $baris['nama_kab']; ?></td>
					<td><?php echo $baris['tahun']; ?></td>
					<td><?php echo $baris['Indeks_Pembangunan_Manusia']; ?></td>
          <td><?php echo $baris['sumber_data'] ?></td>
				</tr>
				<?php endforeach; ?>
				</tbody>
			</table>			
		</div>
	</section>
  
  <script type="text/javascript">
    var res_all = {};
    
    $(document).ready(async function() { 
      await d3.svg("/svg/kalbar.svg").then(function(xml) {
        d3.select("#svgEmbed").node().appendChild(xml.documentElement);
      });
      
      d3.select("svg").style("margin", "auto");
      d3.select("svg").style("display", "block");
      
      <?php 
        foreach($hasil_query_ipm as $baris): 
          $y = $baris['tahun'];
          $kab = $baris['nama_kab'];
          $val = $baris['Indeks_Pembangunan_Manusia'];
          echo "if ($y in res_all) { res_all[$y].push(['$kab', $val]); } else { res_all[$y] = [['$kab', $val]]; } ";
        endforeach; 
      ?>
      
      
      <?php 
        $last = $hasil_query_tahun[0]['tahun'];
        echo "render($last, '11');"; 
      ?>
    });
    
    $("#tahun").change(function () {
        var end = this.value;
        render(end, "11");
    });
    
  </script>
  
	<?php require_once('includes/loadresulttable.php') ?>
	<?php require_once('includes/footer.php') ?>
</body>
</html>
